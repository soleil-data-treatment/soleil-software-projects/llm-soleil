# LLM SOLEIL

Synchrotron SOLEIL LLM services

## RAG service (July 2024)

Have a look at [RAG Soleil installation/usage](rag_soleil/README.md)

Basically, to deploy the service, issue:

In case you are being a proxy:
- `PREFIX=/var/lib/llm-rag-soleil`
- `PROXY=http://195.221.10.6:8080`
- `sudo adduser --system --home $PREFIX --force-badname _llm-rag-soleil`
- `sudo -u _llm-rag-soleil rag_soleil/install.sh $PREFIX $PROXY`

In case you are directly on the internet:
- `PREFIX=/var/lib/llm-rag-soleil`
- `sudo adduser --system --home $PREFIX --force-badname _llm-rag-soleil`
- `sudo -u _llm-rag-soleil rag_soleil/install.sh $PREFIX`

Then you should launch the service:
- `source $PREFIX/rag-env-soleil/bin/activate; cd $PREFIX/llm-soleil/rag_soleil; streamlit run app.py`
- firefox http://localhost:8502/

and obtain the landing page

![RAG Soleil service via StreamLit](rag_soleil/streamlit_ragsoleil.png)

---

