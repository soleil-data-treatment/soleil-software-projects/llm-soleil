"""
Nom du fichier : soleil.py
Auteur : HASSANI Jawaheer

Description :
Classe Soleil pour interagir avec le site web  l'entreprise Soleil Synchotron.
Cette classe permet de récupérer le contenu des pages du site, de faire du scrping sur le site et de nettoyer les pages

Utilisation :
Le script est conçu pour être utilisé dans un contexte où il est nécessaire pour extraire des informations  
et les stocker localement ou les traiter.

Dépendances :
requests, bs4, json, urllib, re, matplotlib

"""  
 
 
import requests
from bs4 import BeautifulSoup
import json
from requests.exceptions import RequestException
from urllib.parse import urlparse
import re
import matplotlib.pyplot as plt


class Soleil:
    def __init__(self, base_url):
        self.base_url = base_url


    # Obtenir le nombre de liens sur une page:
    def get_link(self, url_page):
        """ Récupère tous les liens présent sur une page du site de Soleil."""

        # Liste pour stocker les liens
        links = []
        file_extension = ['/download', '?download', '.pdf', '.doc', '.docx', '.xls', '.xlsx', '.ppt', '.pptx', '.zip', '.rar', 'mailto:', '.png']
        image_extensions = ['.jpg', '.jpeg', '.png', '.gif', '.bmp','.tiff', '.tif','.ico', '.svg', '.webp', '.heif', '.heic', '.raw', '.cr2', '.nef', '.orf', '.arw','.dng']
        black_list = file_extension + image_extensions
        # Ajoutez ici les mots clés ou expressions à exclure des URL
        excluded_keywords = ['annuaire', 'videos', 'images', 'image', 'video', 'publications', 'highlights']

        # le requetage :
        try:
            response = requests.get(url_page)
            response.raise_for_status() 
        
            # On parse la page html
            soup = BeautifulSoup(response.content, 'html.parser')

            # recherche tous les liens hypertexte :
            for link in soup.find_all('a'):
                href = link.get('href')
                if href:
                    full_link = requests.compat.urljoin(url_page, href)
                    # vérification si le lien ne mène pas à un téléchargement et ne contient pas les mots clés exclus
                    if not any(x in full_link.lower() for x in black_list) and not any(keyword in full_link.lower() for keyword in excluded_keywords):
                        links.append(link.get('href'))

        except RequestException as e:
            print(f"Erreur lors de la récupération des liens : {e}")
        return links
    
    def extract_text(self, html_content):
        """ Extrait le texte à partir du contenu HTML. 
        Args:
            - html_contents (page HTML) : une page HTML.
        Returns : 
            - (str) le texte contenue dans la page HTML
        """
        if '<' in html_content and '>' in html_content:
            soup = BeautifulSoup(html_content, 'html.parser')
            content_text = soup.get_text(separator=' ', strip=True)

            # On élimine les choses indésirable
            regex_pattern = r'\[\[{"fid":.*?\}\]\]'
            content_text  = re.sub(regex_pattern, '', content_text) # marche super bien !
            return content_text
        else:
            return html_content

    # Lecture du contenue d'une page : 
    def get_page_content(self, page_url):
        """ Récupère le texte contenue textuelle d'une page du site de soleil(hors intranet).

        Args: 
            - page_url : l'url de la page
        
        Returns:
            - page : une chaine de charactères contenant le contenue textuelles de la page.
         
         """
        
        try:

            # Le requetage :
            response = requests.get(page_url)
            response.raise_for_status()

            # On parse le page HTML
            soup = BeautifulSoup(response.text, 'html.parser')
            #content = soup.find('div', id='content')
            title_tag = soup.find('title')
            title = title_tag.get_text().strip() if title_tag else page_url # Si pas de titre retourne l'url comme titre
            content = self.extract_text(response.text) if response.text else "Pas de contenue disponible"
            return title, content
        
        except RequestException as e:

            print(f"Erreur : Impossible de récupérer le contenue de le page: {e}")
            return 'None', 'None' 

    
    # Chargement de tous les pages du site :
    def get_all(self):
        """ Récupère les contenus textuels de toutes les pages du site de Soleil (hors intranet). """

        visited_urls = set()  # Stocke les url visitées
        urls_to_visit = [self.base_url]  # Initialise avec l'URL de base
        all_pages = {}  # Pour stocker les contenus récupérés
        compt = 0
        
        base_domain = urlparse(self.base_url).netloc

        while urls_to_visit:
            current_url = urls_to_visit.pop(0)
            if current_url not in visited_urls: # si pas visitée contact@yvon.eu
                print(current_url)
                compt = compt + 1 
                print(compt)
                visited_urls.add(current_url) # on l'ajoute au page visitée
                # On récupère le contenue textelles de la page actuelle:
                title, content = self.get_page_content(current_url)
                all_pages[title] = content

                # Ajout des nouveau liens :
                links = self.get_link(current_url) #On récupère tout les hyperliens.
                for link in links:
                    if link:
                        full_link = requests.compat.urljoin(current_url, link)
                        link_domain = urlparse(full_link).netloc # récupère le nom de domaine
                        if link_domain == base_domain and full_link not in visited_urls:
                            urls_to_visit.append(full_link)

        return all_pages


    # Enregistreme du json:
    def save_to_json(self, data, output_path):
        """ Enregistre les données dans un json """
        with open(output_path, 'w') as file:
            json.dump(data, file)
        print(f"Données enregistrées dans le fichier {output_path}")
    
    
    def read_page(self, json_data, by = 'index', index=None, title=None):
        """ Lit le contenu d'une page spécifique dans les données JSON. """
        if by == 'index':
            try:
                values = list(json_data.values())
                return values[index].strip()
            except Exception as e:
                print(f"Erreur lors de la récupération de la page: {e}")
        else:
            if title in json_data:
                return json_data[title].strip()
            else:
                print("Ce titre ne correspond à aucune page !!!")


    def plot_length_histogram(self, json_data, specific_value = 500):
        """ Affiche un histogramme du nombre de caractères des pages. """
        lengths = []
        values = list(json_data.values())
        for page in values:
            lengths.append(len(page.strip()))

        plt.figure(figsize=(8, 6))
        plt.bar(range(len(lengths)), sorted(lengths))
        plt.title("Histogramme des taille(en caractères) des pages")
        plt.xlabel('Page')
        plt.ylabel('Taille(caractères)')
        plt.axhline(y=specific_value, color='r', linestyle='--', label=f'Valeur spécifique ({specific_value})')
        plt.show()

    def clean_data(self, json_data, output_path, threshold=500):
        """ Supprime les pages vides ou inférieures à un certain seuil de caractères et sauvegarde les données nettoyées. """
        cleaned_json = []
        for key, value in json_data.items():
            if len(value.strip()) > threshold:
                cleaned_json.append({key: value})

        try:
            with open(output_path, 'w', encoding='utf-8') as json_file:
                json.dump(cleaned_json, json_file, ensure_ascii=False)
            print("Enregistrement Fait !!!")
        except Exception as e:
            print(f"Erreur lors de l'enregistrement du fichier : {e}")

    

