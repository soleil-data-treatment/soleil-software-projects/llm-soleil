import json
import numpy as np
import matplotlib.pyplot as plt


def clean_json(json_data, output_filename, char_threshold = 500):
    """ Elimine les items du dictionnaire en fonction d'un seuil de nombre de caractères.

       Args : 
           - json_data (dict): JSON contenant les items (pages).
           - char_threshold (int): Seuil limite de nombre de caractères.
           - output_filename (str): Le chemin où enregistrer le nouveau fichier. 

        Returns : 
            - Aucun retour 
    """
    
    # Transformation du JSON et suppression des pages en fonction du seuil
    cleaned_data = [
        {title: '#' + ' ' + title + content}
        for title, content in json_data.items()
        if len(content) > char_threshold
    ]

    # Enregistrement du nouveau JSON
    with open(output_filename, 'w', encoding='utf-8') as output_file:
        json.dump(cleaned_data, output_file, indent=2)



# Nettoyage des données confluences
json_filename = "Data/data_confluence_pages.json"
with open(json_filename, 'r', encoding='utf-8') as json_file:
        json_data = json.load(json_file)


"""# Calcul du seuil basé sur la taille des valeurs
page_size = [len(value) for value in json_data.values()]
#print(f"Nombre d'éléments analysés : {len(page_size)}")

# Distribution :
quartiles = np.percentile(page_size , [25, 50, 75])
moyenne = np.mean(page_size)
ecart_type = np.std(page_size )

quartiles, moyenne, ecart_type
print(f"Premier quartile : {quartiles[0]}")
print(f"Second quartile : {quartiles[1]}")
print(f"Troisième quartile : {quartiles[2]}")
print(f"Moyenne: {moyenne}")
print(f"écart-type: {ecart_type}")"""
output_filename = "Data/cleaned_confluence_pages_v1.json"
clean_json(json_data, output_filename)


