"""
Nom du fichier : document_processor.py
Auteur : HASSANI Jawaheer

Description :
1. La classe `MarkdownProcessor` fournit plusieurs fonctionnalités pour manipuler les documents Markdown :
    - `split_markdown_sections` : Divise un document Markdown en sections individuelles en utilisant les titres.
    - `divide_sentence` : Divise une longue phrase en sous-phrases en fonction d'une limite de tokens.
    - `divide_section` : Divise une section en phrases tout en respectant une limite de tokens.
    - `group_sections` : Regroupe les sections en parties ne dépassant pas une limite de tokens.

Dépendances :
re : transformers, SpaCy et re.
"""

import re

class MarkdownProcessor:
    def __init__(self, tokenizer, nlp_model, token_limit=800):
        """
        Initialise un processeur de markdown.

        Args:
            tokenizer (function): Fonction de tokenisation utilisée pour compter les tokens.
            nlp_model (object): Modèle NLP pour découper les phrases et les sections.
            token_limit (int): Limite maximale de tokens pour chaque sous-section.
        """
        self.tokenizer = tokenizer
        self.nlp_model = nlp_model
        self.token_limit = token_limit

    def split_markdown_sections(self, markdown_content):
        """
        Divise le contenu Markdown en sections.

        Args:
            markdown_content (str): Le contenu de la page écrit en markdown.

        Returns:
            sections (list): Liste des sections de la page.
        """
        pattern = re.compile(r'\n(#+) ')
        matches = list(pattern.finditer(markdown_content))
        sections = []

        if not matches or matches[0].start() != 0:
            if matches:
                first_section_end = matches[0].start()
            else:
                first_section_end = len(markdown_content)

            sections.append(markdown_content[:first_section_end].strip())

        for i in range(len(matches)):
            start_of_section = matches[i].start()
            if i < len(matches) - 1:
                end_of_section = matches[i + 1].start()
            else:
                end_of_section = len(markdown_content)

            section_content = markdown_content[start_of_section:end_of_section].strip()
            sections.append(section_content)

        return sections

    def divide_sentence(self, sentence):
        """
        Divise une phrase en sous-phrases ne dépassant pas une limite de tokens.

        Args:
            sentence (str): La phrase à diviser.

        Returns:
            list: Liste des sous-phrases.
        """
        doc = self.nlp_model(sentence)
        words = [token.text_with_ws for token in doc]
        sub_sentences = []
        current_sub_sentence = ""

        for word in words:
            test_sub_sentence = current_sub_sentence + word
            test_sub_sentence_size = len(self.tokenizer.tokenize(test_sub_sentence))

            if test_sub_sentence_size > self.token_limit:
                sub_sentences.append(current_sub_sentence.strip())
                current_sub_sentence = word
            else:
                current_sub_sentence += word

        if current_sub_sentence.strip():
            sub_sentences.append(current_sub_sentence.strip())

        return sub_sentences

    def divide_section(self, section):
        """
        Divise une section en sous-sections basées sur des phrases.

        Args:
            section (str): La section à diviser.

        Returns:
            list: Liste des sous-sections.
        """
        doc = self.nlp_model(section)
        sentences = [sentence.text for sentence in doc.sents]
        sections = []
        current_sub_section = ""

        for sentence in sentences:
            test_sub_section = current_sub_section + " " + sentence if current_sub_section else sentence
            test_sub_section_size = len(self.tokenizer.tokenize(test_sub_section))

            if test_sub_section_size > self.token_limit:
                sections.append(current_sub_section.strip())
                current_sub_section = sentence
            else:
                current_sub_section += " " + sentence if current_sub_section else sentence

        if current_sub_section.strip():
            sections.append(current_sub_section.strip())

        return sections

    def group_sections(self, sections):
        """
        Groupe les sections en parties ne dépassant pas une limite de tokens.

        Args:
            sections (list): Les sections à grouper.

        Returns:
            dict: Un dictionnaire contenant les parties.
        """
        parts = {}
        part_num = 1
        current_part = ""
        section_added = False

        for section in sections:
            section_added = False

            if len(self.tokenizer.tokenize(section)) > self.token_limit:
                sub_sections = self.divide_section(section)
                for sub_section in sub_sections:
                    parts[f'part_{part_num}'] = sub_section.strip()
                    part_num += 1
                section_added = True
            else:
                test_current_part = current_part + " " + section if current_part else section
                test_current_part_size = len(self.tokenizer.tokenize(test_current_part))

                if test_current_part_size > self.token_limit:
                    parts[f'part_{part_num}'] = current_part.strip()
                    part_num += 1
                    current_part = section
                    section_added = False
                else:
                    current_part += " " + section if current_part else section
                    section_added = True

        if current_part and not section_added:
            if len(self.tokenizer.tokenize(current_part)) > self.token_limit:
                sub_sections = self.divide_section(current_part)
                for sub_section in sub_sections:
                    parts[f'part_{part_num}'] = sub_section.strip()
                    part_num += 1
            else:
                parts[f'part_{part_num}'] = current_part.strip()
        elif current_part and section_added:
            parts[f'part_{part_num}'] = current_part.strip()

        return parts
