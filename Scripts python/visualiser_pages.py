import json
import tkinter as tk
from tkinter import messagebox

def page_suivante():
    """ Passe à la page suivante. """
    global index
    if index < len(json_file) - 1:
        index += 1
        afficher_page()

def page_precedente():
    """ Retourne à la page précédente. """
    global index
    if index > 0:
        index -= 1
        afficher_page()

def afficher_page():
    """ Affiche la page actuelle en se basant sur l'index global. """
    global item
    item = json_file[index]
    chaine = item.get('page_complete', '')

    texte.config(state=tk.NORMAL)
    texte.delete('1.0', tk.END)
    texte.insert(tk.END, chaine)
    texte.config(state=tk.DISABLED)

# Configuration de la fenêtre 
fenetre = tk.Tk()
fenetre.title("Lecteur de page")

# Création des boutons
suivant_bouton = tk.Button(fenetre, text="Page Suivante", command=page_suivante)
suivant_bouton.pack(side=tk.RIGHT, padx=10)

precedent_bouton = tk.Button(fenetre, text="Page Précédente", command=page_precedente)
precedent_bouton.pack(side=tk.LEFT, padx=10)

# Création de la zone de texte 
texte = tk.Text(fenetre, wrap=tk.WORD, height=80, width=150)
texte.pack(pady=10)

# Initialisation de l'indice et de la liste des items
index = 0

# Chargement et affichage des données JSON
try:
    json_filename = "/home/hassanij/Documents/Data/pages_confluences_cleaned_v2.json"
    with open(json_filename, 'r', encoding='utf-8') as file:
        json_file = json.load(file)
    afficher_page()
except IOError as e:
    messagebox.showerror("Erreur de Lecture", f"Impossible de lire le file : {e}")
    fenetre.destroy()

# Lancement de la boucle principale 
fenetre.mainloop()
