import json

def rmv_empt_item(data, name_json):
    """ Elimine les items vides d'un json  

    Args:
        - data (json): json contenant les pages.
        - name_json (str): nom du json aprés nettoyage

    Returns:
        - cleaned_data (json): le json nettoyé des pages vides
    """

    # Filtrage des items avec content vide, composé uniquement d'espaces ou de '\n'
    cleaned_data = [item for item in data if item['content'].strip()]

    # Enregistrement du json netoyer
    with open(name_json, 'w') as file:
        json.dump(cleaned_data, file, indent=2)
    return "Nettoyage effectué"
