"""
Nom du fichier : extract.py
Auteur : HASSANI Jawaheer

Description :
1. La classe `QAParser` permet d'extraire des questions, des réponses et des paires de questions-réponses à partir d'un texte donné.
2. La classe 'JSONManager' permet pour gérer les opération lées à la modification d'un fichier JSON.

Dépendances :
re, os et json
"""


import re
import os
import json

class QAParser:
    @staticmethod
    def extract_questions(text):
        """
        Extrait les questions dans texte.
        """
        pattern = r"Question\s*\d+\s*:\s*(.*?)(?=\nQuestion\s*\d+\s*: |\Z)"
        questions = re.findall(pattern, text, re.DOTALL)
        questions_list = [{"question": q.strip()} for q in questions]
        return questions_list

    @staticmethod
    def extract_response(text):
        """
        Extrait la réponse unique dans le texte.
        """
        pattern = r"Réponse\s*:\s*(.*)"
        match = re.search(pattern, text, re.DOTALL | re.IGNORECASE)
        if match:
            return match.group(1).strip()
        else:
            return text

    @staticmethod
    def extract_qa(text, part):
        """
        Extrait les paires de questions-réponses d'un texte.
        """
        pattern = r"Question\s*\d+\s*:\s*(.*?)\nRéponse\s*\d+\s*:\s*(.*?)(?=\nQuestion\s*\d+\s*\Z)"
        questions_responses = re.findall(pattern, text, re.DOTALL)
        questions_responses_list = [{"question": q.strip(), "context": part, "response": r.strip()} for q, r in questions_responses if q.strip() and r.strip()]
        return questions_responses_list



class JSONManager:

    @staticmethod
    def add_to_json(questions_responses_list, path_file):
        """
        Ajoute des paires de questions-réponses à un fichier JSON.

        Args:
            questions_responses_list (list): Liste de dictionnaires contenant des paires de questions-réponses.
            path_file (str): Chemin vers le fichier JSON.
        """
        if not os.path.exists(path_file):
            # Crée le fichier JSON s'il n'existe pas
            with open(path_file, 'w', encoding='utf-8') as file:
                json.dump([], file, ensure_ascii=False, indent=4)

        try:
            with open(path_file, 'r', encoding='utf-8') as file:
                data_json = json.load(file)
        except json.JSONDecodeError: # Initialise data_json comme une liste vide si le fichier est vide
            data_json = []

        # Verifie si le json est valide(contient une liste)
        if not isinstance(data_json, list):
            raise ValueError(f"Le JSON '{path_file}' n'est pas valide.")

        # Ajout des q-r
        updated_data = data_json + questions_responses_list  # Concatène les listes

        # Sauvegarde du fichier JSON mis à jour
        with open(path_file, 'w', encoding='utf-8') as file:
            json.dump(updated_data, file, ensure_ascii=False, indent=4)

