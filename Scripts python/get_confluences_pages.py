import os
import requests
from bs4 import BeautifulSoup
import json
from llama_index import download_loader
#from llama_hub.confluence import ConfluenceReader
import argparse
import logging
from requests.exceptions import RequestException

#a anvoyer sur le terminal
""" python scripts/get_confluences_pages.py --username "jawaheer.hassani@synchotron-soleil.fr" --api_token "ATATT3xFfGF0aXBdETmGH_bx2c5gkUFCDv4c7XJtCUOvLWVnEXoLQw3LebGeQnvQyp9zt7j9RiV_Vv0Mk4OFUW3-LWSw-6IVeATe2GGUTVHhJZQ12vFtuUDDkjzdtS4fuJkagZmsKNnl4JVhUBXLhMcEXe4BDvSY-oveQJMY2lzO2ZZ_d5QTDjQ=12BACFDC" --base_url "https://confluence.synchrotron-soleil.fr"
"""

# Configuration du logging
logging.basicConfig(level=logging.INFO, format='%(asctime)s - %(levelname)s - %(message)s')

def parse_arguments():
    """Parse les arguments de la ligne de commande."""
    parser = argparse.ArgumentParser(description="Script pour récupérer et sauvegarder les pages Confluence de SOLEIL.")
    parser.add_argument("--username", required=True, help="Nom d'utilisateur pour l'authentification Confluence(SOLEIL).")
    parser.add_argument("--api_token", required=True, help="Token API pour l'authentification Confluence (Altassian).")
    parser.add_argument("--base_url", required=True, help=" URL de base de Confluence SOLEIL.")
    return parser.parse_args()

def set_global_config(username, api_token, base_url):
    """ Configure les variables globales pour l'authentification et l'URL de base. 

    Args:
        - username (str) :  identifiants(ou mail) pour accéder au confluence .
        - api_token (str):  token pour accéder à confluence(altassian).
        - base_url (str) :  adresse du serveur du confluence

    Returns:
        Aucun retour.
    """
    os.environ['CONFLUENCE_USERNAME'] = username
    os.environ['CONFLUENCE_API_TOKEN'] = api_token
    global confluence_url
    confluence_url = base_url

def get_space_keys(base_url):
    """ Récupère les clés des espaces Confluence

    Args: 
        - base_url (str) : adresse du serveur confluence 

    Returns:
        - liste des nom d'espaces
    """
    spaces_endpoint = "/rest/api/space"
    try:
        response = requests.get(base_url + spaces_endpoint)
        response.raise_for_status()
        space_data = response.json()["results"]
        return [space["key"] for space in space_data]
    except RequestException as e:
        logging.error(f"Erreur lors de la connexion au Confluence de SOLEIL: {e}")
        return []

def get_pages_content(space_keys):
    """ Récupère le contenu des pages pour chaque espace clé donné. 

    Args: 
        - space_keys (list) :  list des noms d'espaces à récupérer.

    Returns:
        - dict_pages (dict) : dictionnaire contenant les pages avec les titre comme clés.
     
     """
    ConfluenceReader = download_loader('ConfluenceReader')
    reader = ConfluenceReader(confluence_url)
    dict_pages = {}
    for space_key in space_keys:
        try:
            pages = reader.load_data(space_key=space_key)
            for page in pages:
                title = page.extra_info['title']
                content = page.text
                text_content = extract_text(content)
                dict_pages[title] = text_content
        except Exception as e:
            logging.error(f"Erreur lors de la récupération des pages pour l'espace {space_key}: {e}")
    return dict_pages

def extract_text(html_content):
    """ Extrait le texte à partir du contenu HTML. 
    Args:
        - html_contents (page HTML) : une page HTML.
    Returns : 
        - (str) le texte contenue dans la page HTML
    """
    if '<' in html_content and '>' in html_content:
        soup = BeautifulSoup(html_content, 'html.parser')
        return soup.get_text(separator=' ', strip=True)
    else:
        return html_content

def save_data_to_json(data, filename):
    """Enregistre les données dans un fichier JSON."""
    with open(filename, "w") as file:
        json.dump(data, file)
    logging.info(f"Données enregistrées dans le fichier {filename}")

#Execution :
def main():
    args = parse_arguments()
    set_global_config(args.username, args.api_token, args.base_url)
    space_keys = get_space_keys(args.base_url)
    if space_keys:
        logging.info("Récupération des noms d'espaces: Fait")
        dict_pages = get_pages_content(space_keys)
        save_data_to_json(dict_pages, "data_confluence_pages.json")
        logging.info("Données enregistrées avec succès.")
    else:
        logging.error("Aucun espace trouvé. Vérifiez les détails de votre configuration.")

if __name__ == "__main__":
    main()
