import json
import tkinter as tk
from tkinter import messagebox

# Initialisation des variables globales
items_conserves = []
index = 0
item = None

def garder():
    """ Ajoute l'élément actuel à la liste des éléments conservés et affiche la page suivante. """
    items_conserves.append(item)
    afficher_page_suivante()

def supprimer():
    """ Affiche la page suivante sans conserver l'élément actuel. """
    afficher_page_suivante()

def afficher_page_suivante():
    """ Incrémente l'indice et affiche la page suivante ou ferme la fenetre si c'est la fin. """
    global index
    index += 1
    if index < len(json_file):
        afficher_page()
    else:
        enregistrer_et_quitter()

def afficher_page():
    """ Affiche la page actuelle en se basant sur l'indice global. """
    global item
    item = json_file[index]
    chaine = item.get('page_complete', '')

    texte.config(state=tk.NORMAL)
    texte.delete('1.0', tk.END)
    texte.insert(tk.END, chaine)
    texte.config(state=tk.DISABLED)

def enregistrer_et_quitter():
    """ Enregistre les éléments conservés dans un file JSON et ferme la fenêtre. """
    try:
        with open('/home/hassanij/Documents/Data/pages_confluences_cleaned_v2.json', 'w', encoding='utf-8') as output_file:
            json.dump(items_conserves, output_file, indent=2)
    except IOError as e:
        messagebox.showerror("Erreur d'Enregistrement", f"Une erreur est survenue lors de l'enregistrement du file : {e}")
    finally:
        fenetre.destroy()

# Configuration de la fenêtre Tkinter
fenetre = tk.Tk()
fenetre.title("Surveiller et Punir")

# Création des boutons
garder_bouton = tk.Button(fenetre, text="Garder", command=garder)
garder_bouton.pack(side=tk.LEFT, padx=10)

supprimer_bouton = tk.Button(fenetre, text="Supprimer", command=supprimer)
supprimer_bouton.pack(side=tk.LEFT, padx=10)

# Création de la zone de texte pour afficher la page
texte = tk.Text(fenetre, wrap=tk.WORD, height=80, width=150)
texte.pack(pady=10)

# Chargement et affichage des données JSON
try:
    json_filename = "/home/hassanij/Documents/Data/pages_confluences_cleaned_v1.json"
    with open(json_filename, 'r', encoding='utf-8') as json_file:
        json_file = json.load(json_file)
except IOError as e:
    messagebox.showerror("Erreur de Lecture", f"Impossible de lire le file : {e}")
    fenetre.destroy()
    exit()

afficher_page()

# Lancement de la boucle principale de l'application
fenetre.mainloop()
