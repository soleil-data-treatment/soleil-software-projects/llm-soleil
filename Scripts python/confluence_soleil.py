"""
Nom du fichier : confluence_soleil.py
Auteur : HASSANI Jawaheer

Description :
Classe ConfluenceSoleil pour interagir avec l'API Confluence de l'entreprise Soleil.
Cette classe permet de récupérer le contenu des pages Confluence, lister les espaces disponibles,
extraire le texte à partir du contenu HTML, et enregistrer les données récupérées.

Utilisation :
Le script est conçu pour être utilisé dans un contexte où l'interaction avec Confluence
est nécessaire pour extraire des informations et les stocker localement ou les traiter.

Dépendances :
requests, bs4, json, os et llama_index
""" 
 
 
import requests
from bs4 import BeautifulSoup
import json
import os
#from llama_index.core import download_loader à activer si on veut retélécharger ConflunceReader
from requests.exceptions import RequestException
from llama_index.readers.confluence import ConfluenceReader 

#Téléchargement de confluence loader :  
#ConfluenceReader = download_loader('ConfluenceReader')


class ConfluenceSoleil:
    def __init__(self, base_url, username, api_token): 
        self.base_url = base_url
        self.username = username
        self.api_token = api_token

    
    def get_page_content(self, page_url):
        """ Récupère le texte contenue textuelle dans un page confluence SOLEIL.

        Args: 
            - page_url : l'url de la page
        
        Returns:
            - page : une chaine de charactères contenant le contenue textuelles de la page.
         
         """
        #Connexion à la page confluence
        response = requests.get(page_url)
        
        if response.status_code == 200:
            soup = BeautifulSoup(response.text, 'html.parser')
            # chargement du contenue markdown:
            content = soup.find('div', id='content')
            return content.get_text().strip() if content else "Pas de contenue disponible"
        else:
            return f"Erreur : Impossible de récupérer la page, code de statut {response.status_code}" 


    def get_space_keys(self):
        """ Récupère les clés des espaces Confluence

        Args: 
            - None

        Returns:
            - liste des nom d'espaces
        """
        spaces_endpoint = "/rest/api/space"
        try:
            response = requests.get(self.base_url + spaces_endpoint)
            response.raise_for_status()
            space_data = response.json()["results"]
            return [space["key"] for space in space_data]
        except RequestException as e:
            print(f"Erreur lors de la connexion au Confluence de SOLEIL: {e}")
        return []


    def extract_text(self, html_content):
        """ Extrait le texte à partir du contenu HTML. 
        Args:
            - html_contents (page HTML) : une page HTML.
        Returns : 
            - (str) le texte contenue dans la page HTML
        """
        if '<' in html_content and '>' in html_content:
            soup = BeautifulSoup(html_content, 'html.parser')
            return soup.get_text(separator=' ', strip=True)
        else:
            return html_content


    def get_pages_in_space(self, name_space):
        """ Récupère les pages associées à un nom d'espaces données

        Args: 
            - space_keys (list) :  list des noms d'espaces à récupérer.

        Returns:
            - dict_pages (dict) : dictionnaire contenant les pages avec les titre comme clés.
        """
        
        #Mise en places des variables d'environnement :
        os.environ['CONFLUENCE_USERNAME'] = self.username
        os.environ['CONFLUENCE_API_TOKEN'] = self.api_token
        reader = ConfluenceReader(self.base_url)
        dict_pages = {}
        try:
            pages = reader.load_data(space_key=name_space)
            for page in pages:
                title = page.extra_info['title']
                content = page.text
                text_content = self.extract_text(content)
                dict_pages[title] = text_content
        except Exception as e:
            print(f"Erreur lors de la récupération des pages pour l'espace {name_space}: {e}")
        return dict_pages
    

    def get_all_pages(self):
        """ Récupère toutes les contenues contextuelles de toutes les pages disponible 
        
        Args: 
            - None

        Returns
            - dict_pages (dict) : dictionnaire contenant les pages avec les titre comme clés.: 
        """
        #Mise en places des variables d'environnement :
        os.environ['CONFLUENCE_USERNAME'] = self.username
        os.environ['CONFLUENCE_API_TOKEN'] = self.api_token
        reader = ConfluenceReader(self.base_url)
        names_space = self.get_space_keys()
        dict_pages = {}
        for name_space in names_space:
            try:
                pages = reader.load_data(space_key=name_space)
                for page in pages:
                    title = page.extra_info['title']
                    content = page.text
                    text_content = self.extract_text(content)
                    dict_pages[title] = text_content
            except Exception as e:
                print(f"Erreur lors de la récupération des pages: {e}")
        return dict_pages
    

    def save_data_to_json(self, data, filename):
        """Enregistre les données dans un fichier JSON."""
        with open(filename, "w") as file:
            json.dump(data, file)
        print(f"Données enregistrées dans le fichier {filename}")
    

    def read_page(self, data, by = 'index', index=None, title=None):
        """Lit une page conteneu dans un json"""
        if by == 'index':
            try:
                item = data[index]
                return list(item.values())[0] 
            except Exception as e:
                print(f"Erreur lors de la récupération de la page: {e}")
        elif by == 'title':
            # On recherche l'item correspondnat au titre :
            for item in data:
                if title in item:
                    return item[title]
            print("Ce titre ne correspond à aucun contenue !!!")

            
    def clean_pages(self, json_data, output_filename, char_threshold = 500):
        """ Elimine les items du dictionnaire en fonction d'un seuil de nombre de caractères.

            Args : 
                - json_data (dict): JSON contenant les items (pages).
                - char_threshold (int): Seuil limite de nombre de caractères.
                - output_filename (str): Le chemin où enregistrer le nouveau fichier. 

            Returns : 
                - Aucun retour 
        """
    
        # Transformation du JSON et suppression des pages en fonction du seuil
        cleaned_data = [
            {title: '#' + ' ' + title + content}
            for title, content in json_data.items()
            if len(content) > char_threshold
    ]

        # Enregistrement du nouveau JSON
        with open(output_filename, 'w', encoding='utf-8') as output_file:
           json.dump(cleaned_data, output_file, indent=2, ensure_ascii=False)
