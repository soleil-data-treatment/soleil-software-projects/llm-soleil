import numpy as np
import matplotlib.pyplot as plt

import numpy as np
import matplotlib.pyplot as plt

def positional_encoding(position, d_model):
    # Crée un tableau pour l'encodage positionnel
    encoding = np.zeros((position, d_model))
    # Calcule le diviseur pour l'échelle
    scale = np.power(10000, 2 * np.arange(d_model // 2) / d_model)

    # Calcule les valeurs d'encodage positionnel
    encoding[:, 0::2] = np.sin(np.arange(position).reshape(-1, 1) / scale)
    encoding[:, 1::2] = np.cos(np.arange(position).reshape(-1, 1) / scale)

    return encoding

# Exemple d'utilisation
position = 3  # Nombre de positions dans la séquence
d_model = 10  # Dimension de l'embedding

pos_encoding = positional_encoding(position, d_model)

# Visualisation de l'encodage positionnel pour chaque position
plt.figure()
for pos in range(len(pos_encoding)):
    plt.plot(pos_encoding[pos], label=f'Position {pos}')
plt.xlabel('Dimension')
plt.ylabel('Magnitude')
plt.title('L\'encodage positionnel pour différentes position')
plt.legend()
plt.show()

