### Import Librairies
import os
import streamlit as st
from streamlit_chat import message
from rag_soleil import RAGSoleil


st.set_page_config(page_title="CharRag - Synchrotron Soleil")

def display_messages():
    st.subheader("Chat")
    for i, (msg, is_user, context) in enumerate(st.session_state["messages"]):
        message(msg, is_user=is_user, key=str(i))
        if not is_user and context:
            if st.button("Show context", key=f"context_button_{i}"):
                st.write(context)
    st.session_state["thinking_spinner"] = st.empty()

def process_input():
    if st.session_state["user_input"] and len(st.session_state["user_input"].strip()) > 0:
        user_text = st.session_state["user_input"].strip()
        with st.session_state["thinking_spinner"], st.spinner("Thinking in progress..."):
            agent_text, context = st.session_state["assistant"].answer(user_text)

        st.session_state["messages"].append((user_text, True, None))
        st.session_state["messages"].append((agent_text, False, context))

def read_and_save_files():
    if st.session_state["uploaded_files"]:
        st.session_state["assistant"] = RAGSoleil(
            model_embedding_path="models/gte-Qwen2-1.5B-instruct",
            normalize_embeddings=True,
            model_path="models/Mistral-7B-Instruct-v0.2",
            max_new_tokens=2048,
            temperature=0.7,
            device_id=0,
            do_sample=False
        )
        st.session_state["messages"] = []
        st.session_state["user_input"] = ""

        with st.session_state["ingestion_spinner"], st.spinner("Ingesting uploaded files"):
            uploaded_files = [(file.name, file.read()) for file in st.session_state["uploaded_files"]]
            documents = st.session_state["assistant"].load_from_uploaded_documents(uploaded_files)
            chunks = st.session_state["assistant"].split_document(documents, chunk_size=500, chunk_overlap=20)
            st.session_state["assistant"].create_database(chunks)
            st.session_state["assistant"].create_retriever(k=5)
            st.session_state["assistant"].create_qa_chain()
    else:
        st.error("No files have been uploaded")

def page():
    if "messages" not in st.session_state:
        st.session_state["messages"] = []
    if "assistant" not in st.session_state:
        st.session_state["assistant"] = None
    if "uploaded_files" not in st.session_state:
        st.session_state["uploaded_files"] = []

    st.image("chatragsoleil.png", width=400)
    st.header("Welcome to ChatRag - Synchrotron Soleil")

    st.subheader("Upload documents to narrow our discussion (required)")
    uploaded_files = st.file_uploader("Upload documents (DOCX TXT PDF)", accept_multiple_files=True)
    if uploaded_files:
        st.session_state["uploaded_files"].extend(uploaded_files)

    if st.button("Start the RAG process"):
        read_and_save_files()

    st.session_state["ingestion_spinner"] = st.empty()

    display_messages()
    st.text_input("Enter your question here", key="user_input", on_change=process_input)

if __name__ == "__main__":
    page()
