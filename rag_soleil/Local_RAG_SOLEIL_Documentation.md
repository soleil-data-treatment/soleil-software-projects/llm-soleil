
### Les dépendances

Ci-dessous les dépendances nécessaire pour Le système RAG.
```python
# langchain Librairies
from langchain.text_splitter import RecursiveCharacterTextSplitter
from langchain.vectorstores import FAISS
from langchain.schema import Document as LangchainDocument
from langchain.prompts import PromptTemplate
from langchain.chains import RetrievalQA

# transformers librairies
from transformers import AutoTokenizer, AutoModelForCausalLM, BitsAndBytesConfig, pipeline

# HuggingFace for Langchain librairies
from langchain_huggingface import HuggingFacePipeline, HuggingFaceEmbeddings
from langchain_community.embeddings.fastembed import FastEmbedEmbeddings

# torch librairies 
import torch
from torch.quantization import quantize_dynamic

# streamlit librairies
import streamlit as st  
from streamlit_chat import message

# other useful libraries
import os
import re
import json
from datasets import Dataset
from docx import Document
import pdfplumber
```

#### Librairies Principales

- **Langchain** : Langchain permet de construire des chaînes de traitement de texte complexes. Dans note cas, elle es utilisée pour la gestion des documents, la vectorisation de texte, et la construction de chaînes de questions-réponses.

- **Transformers** : Développée par Hugging Face, elle est essentielle pour l'utilisation des grands modèles de langage(LLM). Nous l'utilisons pour charger et exécuter le modèle chargé de répondre aux requêtes.

- **Streamlit** : Streamlit  est utilisé pour la création d'applications web interactives et la visualisation des données. Nous l'utilisons pour produire l'interface utilisateur du chat.

- **HuggingFace** : HuggingFace  est une plateforme qui permet de partager et de découvrir des modèles de deep learning. Tous les modèles que nous utilisons proviennent de cette plateforme.


### **Retrieval Augmented Generation (RAG)**

Le RAG combine des systèmes de récupération d'informations(principalement de texte) avec de grands modèles de langage (LLM) pour améliorer la précision et la pertinence des textes générés. Plus simplement, l'idée est de fournir à un LLM le contexte qui lui permet de répondre à une question. Un RAG se compose de :

1. **Documents** : Ce sont les documents qui traitent d'un ou de plusieurs sujets particuliers. Ces documents sont ceux dont le contexte sera tiré en fonction de la question (formellement appelée **requête**). Les documents sont découpés en morceaux (**chunks**) en fonction d'un certain nombre de caractères, 1000 dans notre cas.

2. **Vectorization Models** : En général, ce sont des modèles qui servent à vectoriser les chunks. L'idée est de créer une base de données vectorielle.

3. **Vector Databases** : Ce sont des bases de données spécialisées qui stockent et indexent des informations (textes) sous forme de vecteurs, permettant des recherches par similarité pour récupérer des données pertinentes.

4. **Retriever** : Cet élément permet de vectoriser les requêtes et de rechercher dans la base de données les vecteurs correspondants (en en choisissant arbitrairement un certain nombre). Les chunks correspondant aux vecteurs trouvés constituent le contexte qui est fourni au modèle pour générer la réponse.

5. **Prompt** : Celui-ci intègre le contexte récupéré pour formuler la requête au modèle.

6. **Large Language Models (LLMs)** : Le modèle chargé de fournir la réponse à la question en tenant compte du contexte.

 #### **Intérêt du RAG**
 L'avantage d'un tel système est qu'il permet de fournir à un LLM des informations sur lesquelles il n'a pas été initialement entraîné, sans avoir à effectuer un fine-tuning du modèle.
 Pour une description théoriques plus complètes. Voir les liens ci-dessous.
 - https://www.datastax.com/guides/what-is-retrieval-augmented-generation
 - https://medium.com/@tejpal.abhyuday/retrieval-augmented-generation-rag-from-basics-to-advanced-a2b068fd576c

### **Description de RAGSoleil**

La classe `RAGSoleil` met en œuvre un système RAG en suivant les principes  décrits ci-dessus. Cette classe instancie un objet RAG en spécifiant le modèle d'embeddings (`model_embedding_path`), le modèle de langage (`model_path`), ainsi que les paramètres du modèle tels que la température (`temperature`), le nombre maximal de tokens à générer (`max_new_tokens`), l'appareil sur lequel charger les modèles (sur GPU) (`device_id`), et la normalisation des vecteurs (`normalize_embeddings`). Une fois l'objet créé, voici les étapes principales :

1. **Chargement des documents** :
   - On charge les documents via les méthodes `load_from_json`, `load_from_document` ou `load_from_uploaded_documents`.
   - Ensuite, on découpe ces documents en chunks avec la méthode `split_document`.

2. **Vectorisation des documents** :
   - Avec le modèle d'embeddings choisi, on crée un objet `embedding_instance`.
   - La méthode `define_embeddings` vectorise les chunks.

3. **Création de la base de données vectorielle** :
   - La méthode `create_database` crée la base de données vectorielle.

4. **Création du retriever** :
   - La méthode `create_retriever` crée un retriever qui se charge, pour une requête donnée, de trouver les portions de texte les plus susceptibles de correspondre à la requête. Ces portions de texte constituent le contexte.

5. **Création du prompt** :
   - La méthode `define_prompt_template` crée le prompt en y intégrant la requête et le contexte.

6. **Chargement du modèle de langage** :
   - La méthode `define_llm_pipeline` permet de charger le modèle de langage pour la génération de la réponse.

7. **Création de la chaîne RAG** :
   - La méthode `create_qa_chain` crée la chaîne RAG pour intégrer le modèle de langage avec le retriever et le contexte, formant ainsi le système de questions-réponses.

8. **Obtention de la réponse** :
   - La méthode `answer` permet, pour une requête (question) en entrée, de récupérer la réponse du modèle et le contexte d'où est issue la réponse.


### **Interface Utilisateur avec Streamlit**

Pour parfaire le système, une interface utilisateur a été développée avec Streamlit. Cette interface permet de charger des documents, d'interagir avec le modèle et de visualiser les réponses générées.
- L'utilisateur peut importer un ou plusieurs documents aux formats .pdf, .docx ou .txt. Ensuite, en cliquant sur "Start the RAG process", le processus RAG est lancé. Cela crée la base de données vectorielle, la chaîne RAG et le retriever( le système de récupération d’informations. Le chatbot est ainsi mis en place.
- L'utilisateur peut ensuite poser des questions au chatbot, et pour réponse, consulter le contexte d'ou provient l'information en cliquant sur "SHOW CONTEXT".

### **Démarrer Streamlit**
Suivez les instructions suivantes :
**run.sh** : Script d'exécution contenant :
```
#!/bin/bash
streamlit run app.py
```
Assurez-vous que le script run.sh est exécutable.
```
chmod +x run.sh
```
Puis exécutez la commande suivante : 
```
./run.sh
```