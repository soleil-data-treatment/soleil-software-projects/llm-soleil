# Import Librairies
from langchain.text_splitter import RecursiveCharacterTextSplitter
from langchain.vectorstores import FAISS
from transformers import AutoTokenizer, AutoModelForCausalLM, BitsAndBytesConfig, pipeline
from langchain_huggingface import HuggingFacePipeline, HuggingFaceEmbeddings
from langchain.prompts import PromptTemplate
import torch
from langchain.chains import RetrievalQA
import os
import re
from datasets import Dataset
from docx import Document
import pdfplumber

# Définition de la variable d'environnement pour le token Hugging Face
os.environ["HF_TOKEN"] = "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"


class RAGSoleil:
    def __init__(self, model_embedding_path, normalize_embeddings, model_path, max_new_tokens, temperature, device_id, do_sample):
        self.model_embedding_path = model_embedding_path
        self.normalize_embeddings = normalize_embeddings
        self.model_path = model_path
        self.max_new_tokens = max_new_tokens
        self.device_id = device_id
        self.do_sample = do_sample
        self.temperature = temperature
        self.embedding_instance = self.define_embeddings()
        self.vector_store = None
        self.retriever = None
        self.qa_chain = None
        self.prompt_template = self.define_prompt_template()
    
    ### This function initializes an instance of fastEmbedEmbeddings
    def define_embeddings(self):
        """ Initialize an instance of FastEmbedEmbeddings"""
        print("Initialising embedding model...")
        device_embedding = f"cuda:{self.device_id}" if torch.cuda.is_available() else 'cpu'
        return HuggingFaceEmbeddings(
            model_name = self.model_embedding_path,
            model_kwargs = {'device' : device_embedding},
            encode_kwargs = {'normalize_embeddings':self.normalize_embeddings} 
        )

    ### This function creates a prompt with a question and a context
    def define_prompt_template(self):
        """Create a prompt with a context and a question"""
        print("Creating the prompt...")
        return PromptTemplate(template=""" 
        <s> [INST] Tu es un assistant pour des tâches de questions-réponses. Utilises les éléments de contexte récupérés suivants pour  
        répondre à la question. Si tu ne connais pas la réponse, dis simplement que tu ne sais pas. Utilises un maximum de 10 phrases  
        et donnes une réponse détaillée. Réponds préférentiellement en Français, ou en anglais. [/INST] </s>
        [INST] Question: {question}
        Context: {context}
        Answer: [/INST]
            """,
            input_variables=["question", "context"]
        )

    ### This function loads text content from uploaded files
    def load_from_uploaded_documents(self, uploaded_files):
        """Load text content from uploaded files into a list of strings"""
        print("Loading text content...")
        documents = []
        for file_name, file_content in uploaded_files:
            if file_name.endswith('.txt'):
                text = file_content.decode('utf-8')
                documents.append(text)
            elif file_name.endswith('.docx'):
                with open(file_name, 'wb') as f:
                    f.write(file_content)
                doc = Document(file_name)
                text = "\n".join([para.text for para in doc.paragraphs])
                documents.append(text)
            elif file_name.endswith('.pdf'):
                with open(file_name, 'wb') as f:
                    f.write(file_content)
                text = ""
                with pdfplumber.open(file_name) as pdf:
                    for page in pdf.pages:
                        text += page.extract_text()
                documents.append(text)
        return documents
        
    ### This function splits a document into chunks
    def split_document(self, documents, chunk_size, chunk_overlap):
        """Splitting a document into chunks"""
        print("Splitting document into chunks...")
        text_splitter = RecursiveCharacterTextSplitter(chunk_size=chunk_size, chunk_overlap=chunk_overlap)
        return text_splitter.create_documents(documents)
    
    ### This function creates a vector database with FAISS
    def create_database(self, docs):
        """Create a vector database with FAISS"""
        print("Creating FAISS database...")
        self.vector_store = FAISS.from_documents(docs, self.embedding_instance)

    ### This function creates a retriever
    def create_retriever(self, k):
        """Create a retriever for the database"""
        print("Creating retriever...")
        if self.vector_store is None:
            raise ValueError("Vector store is not initialized. Call create_database first.")
        self.retriever = self.vector_store.as_retriever(search_type="similarity", search_kwargs={"k": k})
        return self.retriever
    
    ### This function creates a pipeline for text-generation
    def define_llm_pipeline(self):
        """Loading and creating an LLM pipeline"""
        print("Loading and creating an LLM pipeline...")
        device_id = self.device_id 

        ### The tokenizer
        tokenizer = AutoTokenizer.from_pretrained(self.model_path, trust_remote_code=True)
        tokenizer.pad_token = tokenizer.eos_token
        
        ### with GPU 
        if torch.cuda.is_available() :
            print("Using GPU")
            # Get the right device
            device_id = self.device_id
            
            # Loading the model with 4-bit quantization
            bnb_config = BitsAndBytesConfig(
                load_in_4bit=True,
                bnb_4bit_quant_type="nf4",
                bnb_4bit_compute_dtype=torch.bfloat16,
                bnb_4bit_use_double_quant=False,
            )
            ### The model
            model = AutoModelForCausalLM.from_pretrained(
                self.model_path,
                quantization_config=bnb_config,
                device_map={"": device_id}
            )
        else: ## without GPU
            print("Using CPU")
            model = AutoModelForCausalLM.from_pretrained(self.model_path)
            
        # Define the pipeline for the model
        pipeline_kwargs = {
            "do_sample": self.do_sample,
            "max_new_tokens": self.max_new_tokens
        }
        if self.do_sample:
            pipeline_kwargs["temperature"] = self.temperature

        ### Creation of the pipeline
        pipe = pipeline("text-generation",
                        model=model,
                        tokenizer=tokenizer,
                        **pipeline_kwargs)
        
        llm = HuggingFacePipeline(pipeline=pipe)
        return llm
        
    ### This function creates a RetrievalQA instance
    def create_qa_chain(self):
        """Create a RetrievalQA chain"""
        print("Creating a RetrievalQA chain...")
        if self.retriever is None:
            raise ValueError("Retriever is not initialized. Call create_retriever first.")
        llm_pipeline = self.define_llm_pipeline()
        self.qa_chain = RetrievalQA.from_chain_type(
            llm=llm_pipeline,
            retriever=self.retriever,
            chain_type='stuff',
            chain_type_kwargs={"prompt": self.prompt_template},
            return_source_documents=True
        )
    
    ### This function gets the answer to a question using the QA system
    def answer(self, question):
        """Answer the question"""
        print("Answering the question...")
        if self.qa_chain is None:
            raise ValueError("QA chain is not initialized. Call create_qa_chain first.")
        result = self.qa_chain.invoke({"query": question})
        full_answer = result['result']
        source_documents = result['source_documents']

        # Extract only the answer
        match = re.search(r"Answer:\s*\[\/INST\]\s*(.*)", full_answer, re.DOTALL)
        if match:
            answer = match.group(1).strip()
        else:
            answer = full_answer
        # Extract the context from the source documents
        context = " ".join([doc.page_content for doc in source_documents])
        
        return answer, context
