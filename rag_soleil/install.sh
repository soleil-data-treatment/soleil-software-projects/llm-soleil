#!/bin/bash
#
# Install our RAG Chat SOLEIL
# Usage: install.sh [TARGET_DIR] [PROXY]
#   Default TARGET_DIR is $HOME
#   Default PROXY is the one from /etc/environment (if any)
#
# For instance:
#   sudo adduser --system --home /var/lib/llm-rag-soleil --force-badname _llm-rag-soleil
#   sudo -u _llm-rag-soleil ./install.sh /var/lib/llm-rag-soleil http://195.221.10.6:8080

# where to install the service (e.g. HOME of _llm-rag-soleil account)
PREFIX=$HOME # default target location
GITURL=https://gitlab.com/soleil-data-treatment/soleil-software-projects/llm-soleil

# ------------------------------------------------------------------------------
if [ -f /etc/environment ]; then
. /etc/environment
fi

if [ "x$1" == "x-h" ]; then
  echo "Install our RAG Chat SOLEIL "
  echo "Usage: install.sh [TARGET_DIR] [PROXY]"
  exit 1
fi

if [ "x$1" != "x" ]; then
  mkdir -p $1
  $PREFIX=$1
fi

if [ "x$2" != "x" ]; then
  http_proxy=$2
fi

echo "Installing ChatRAG SOLEIL service in $1"
cd $PREFIX

if [ "x$http_proxy" != "x" ]; then
  git config --global http.proxy  $http_proxy
  git config --global https.proxy $http_proxy
fi
if ! [ -d llm-soleil ]; then
  echo "Installing git from $GITURL"
  git clone $GITURL
else
  echo "OK git llm-soleil already there. Updating."
  (cd llm-soleil; git pull; cd $PREFIX)
fi

if ! [ -d rag-env-soleil ]; then
  echo "Creating base environment."
  python3 -m venv $PREFIX/rag-env-soleil
fi

echo "Install/Update python base environment ($http_proxy)..."
source $PREFIX/rag-env-soleil/bin/activate
if [ "x$http_proxy" == "x" ]; then
  pip3 install torch torchvision torchaudio
else
  pip3 install --proxy $http_proxy torch torchvision torchaudio
fi

echo "Install/Update RAG requirements..."
if [ "x$http_proxy" == "x" ]; then
  pip3 install -r $PREFIX/llm-soleil/rag_soleil/requirements.txt
else
  pip3 install --proxy $http_proxy -r $PREFIX/llm-soleil/rag_soleil/requirements.txt
fi


FILES=/lib/systemd/system/llm-rag-soleil.service
FILE=$PREFIX/llm-soleil/rag_soleil/llm-rag-soleil.service

echo "============================================================================="
echo "Now activate service with"
echo " "
echo "sudo cp $FILE $FILES"
echo "sudo systemctl daemon-reload"
echo "sudo systemctl enable llm-rag-soleil"
echo "sudo systemctl start  llm-rag-soleil"
echo " "
echo "Alternatively start manually:"
echo "source $PREFIX/rag-env-soleil/bin/activate; cd $PREFIX/llm-soleil/rag_soleil; streamlit run app.py"

