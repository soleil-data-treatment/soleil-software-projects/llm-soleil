![Chat RAG Soleil](chatragsoleil.png)

Voici les instructions pour installer et mettre en route le système RAG Soleil.


## Création de l'environnement

Veuillez suivre les commandes suivantes :
- Ouvrez le terminal
- Créez un environnement virtuel en exécutant la commande suivante : 
  ```sh
  python3 -m venv rag-env-soleil
  ```
- Ensuite, activez l'environnement avec la commande :
  ```sh
  source rag-env-soleil/bin/activate
  ```

Ensuite, passons à l'installation des dépendances. Pour des raisons pratiques, il faudra installer PyTorch séparément des autres bibliothèques nécessaires pour le projet.

### Installation de PyTorch et des autres dépendances

L'installation sur Debian se fait comme suit :
1. Assurez-vous que l'environnement est activé.
2. Exécutez la commande suivante pour installer PyTorch :
   ```sh
   pip3 install torch torchvision torchaudio --index-url https://download.pytorch.org/whl/cu118
   ```
   Notez que pour obtenir la commande exacte correspondant à votre système, il faut aller sur le site de [PyTorch](https://pytorch.org) dans la section "Get Started" puis regarder la section "Start Locally".

3. Vérifiez l'installation en exécutant le code suivant dans un script Python ou dans l'interpréteur Python :
   ```python
   import torch
   print(torch.__version__)
   print(torch.cuda.is_available())
   print(torch.cuda.get_device_name(0))
   ```
   Cela devrait donner les résultats suivants :
   ```
   2.3.1+cu118  # Exemple de version de PyTorch
   True
   NVIDIA GeForce RTX 3090
   ```

Ensuite, vous pouvez procéder à l'installation des autres dépendances listées dans le fichier `requirements.txt` en exécutant :
```sh
pip install -r requirements.txt
```

Assurez-vous que votre fichier `requirements.txt` est dans le répertoire courant ou spécifiez son chemin complet.
#### Remarque : 
Les versions recommandées dans `requirements.txt` sont les plus récentes (au 19/07/2024). Cependant, il est possible que des conflits de dépendances surviennent à l'avenir entre certains modules. D'après mon expérience, cela peut arriver notamment entre `transformers`, `accelerate` et `bitsandbytes`. Par exemple, pour charger un modèle Mistral, une version spécifique de `transformers` peut être nécessaire. Ci-dessous, les versions qui fonctionnent bien ensemble :
```python
langchain version: 0.2.9  
transformers version: 4.42.4  
langchain_huggingface version: 0.0.3  
langchain_community version: 0.2.7  
datasets version: 2.20.0  
faiss-cpu version: 1.8.0.post1  
python-docx version: 1.1.2  
pdfplumber version: 0.11.2  
streamlit version: 1.36.0  
streamlit-chat version: 0.1.1  
accelerate version: 0.32.1  
bitsandbytes version: 0.43.1
```
### Téléchargement des modèles

Pour commencer, vous devez créer un répertoire nommé `models`, puis télécharger les modèles dans ce répertoire en utilisant `git clone`. Ci-dessous les instructions.

1. Créez le répertoire `models` :

   ```bash
   mkdir models
   ```

2. Téléchargez les modèles en clonant les dépôts suivants dans le répertoire `models` :

   ```bash
   $ git clone https://huggingface.co/Alibaba-NLP/gte-Qwen2-1.5B-instruct 
   Clonage dans 'gte-Qwen2-1.5B-instruct'...
   remote: Enumerating objects: 60, done.
   remote: Counting objects: 100% (56/56), done.
   remote: Compressing objects: 100% (52/52), done.
   remote: Total 60 (delta 20), reused 0 (delta 0), pack-reused 4 (from 1)
   Dépaquetage des objets: 100% (60/60), 3.65 Mio | 5.79 Mio/s, fait.
   Filtrage du contenu: 100% (2/2), 2.61 Gio | 19.95 Mio/s, fait.
   ```
puis le modèle Mistral AI qui requiert plus d'efforts.

Créez un cpompte sur [https://huggingface.co/](https://huggingface.co/). Un email sera envoyé pour l'activation du compte. Cliquez sur le lien dans l'email d'activation. Ensuite, depuis les "Settings" allez dans "Access Tokens" (colonne de gauche), puis créez un Token. Donnez-lui tous les accès "repositories". Sauvez la chaine de caractères de votre Token ("hf_XX..."). Il servira pour le `git clone`.

Récupérez le modèle Mistral:
   ```bash
   git clone https://huggingface.co/mistralai/Mistral-7B-Instruct-v0.2 
   Clonage dans 'Mistral-7B-Instruct-v0.2'...
   Username for 'https://huggingface.co': farhi
   Password for 'https://farhi@huggingface.co': 
   remote: Enumerating objects: 74, done.
   remote: Counting objects: 100% (74/74), done.
   remote: Compressing objects: 100% (52/52), done.
   remote: Total 74 (delta 35), reused 48 (delta 21), pack-reused 0 (from 0)
   Dépaquetage des objets: 100% (74/74), 481.51 Kio | 1.82 Mio/s, fait.
   Username for 'https://huggingface.co': farhi
   Password for 'https://farhi@huggingface.co': 
   Filtrage du contenu: 100% (7/7), 3.46 Gio | 23.87 Mio/s, fait.
   ```
   
Vous obtiendrez les répertoires suivants : models/gte-Qwen2-1.5B-instruct et models/Mistral-7B-Instruct-v0.2.
Normalement, ces deux dossiers sont déjà configuréz. Mais si vous changez de modèle, ou de nom de dossier, il vous suffit ensuite de spécifier ces deux chemins dans le fichier `app.py`, plus précisément dans la fonction read_and_save_files.
```python
def read_and_save_files():
    if st.session_state["uploaded_files"]:
        st.session_state["assistant"] = RAGSoleil(
            model_embedding_path="models/gte-Qwen2-1.5B-instruct",
            normalize_embeddings=True,
            model_path="models/Mistral-7B-Instruct-v0.2",
            max_new_tokens=2048,
            temperature=0.7,
            device_id=0,
            do_sample=False
        )
```

### Exécution du système RAG

Les fichiers doivent êtres structurés comme suit :
```
/rag_soleil
|-- app.py
|-- rag_soleil.py
|-- run.sh
|-- requirements.txt
```

Pour l’exécution vous pouvez procéder comme suit :
- Activez l'environnement `rag-env-soleil` avec toutes les dépendances installés.
- Ensuite exécutez la commande  : `streamlit run app.py`.
 
 Sinon, faire comme suit:
 - Assurez-vous que le script `run.sh` est exécutable en utilisant la `chmod +x run.sh`
 - Exécutez la commande :  `./run.sh`. 
 
Le service sera alors disponible sur :
- [http://localhost:8502/](http://localhost:8502/)

### Fonctionnement de l'interface utilisateur

![RAG Soleil service via StreamLit](streamlit_ragsoleil.png)

Cette interface permet de charger des documents, d'interagir avec le modèle et de visualiser les réponses générées.

- Importez un ou plusieurs documents aux formats .pdf, .docx ou .txt. Ensuite, en cliquez sur "Start the RAG process", le processus RAG est lancé. 
- Vous pouvez poser des questions au chatbot, lire la réponse, consulter le contexte d'ou provient l'information en cliquant sur "SHOW CONTEXT".


### Note

- Vous pouvez changer les prompts pour la génération des réponses en consultant le fichier `rag_prompt_mistral.txt` qui propose différents prompts pour différents types de réponses souhaitées : courts, concis, etc.
- Notez également que ces prompts sont adaptés au modèle Mistral. Utiliser un autre modèle pour répondre aux questions pourrait s'avérer décevant.


